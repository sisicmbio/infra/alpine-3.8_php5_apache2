FROM registry.gitlab.com/sisicmbio/pdci/image_file_dump:empty AS sql
ENV APP_PROJETO "app_ojs"
COPY sql  /sql/
ENTRYPOINT ["/sbin/tini", "--","/bin/pdci_upd_sql.sh"]

FROM php:5.6.40-fpm-alpine3.8 AS develop

LABEL maintainer="Rafael Roque de Mello <lucas.diedrich@gmail.com>"
#WORKDIR /tmp/

ENV PHP5_DEPS="supervisor dcron apache2 apache2-ssl apache2-utils php5 php5-fpm php5-cli php5-apache2 php5-pgsql php5-zlib \
              php5-json php5-phar php5-openssl php5-curl  php5-pdo_pgsql php5-mcrypt php5-ctype php5-pdo \
              php5-gd php5-xml php5-dom php5-iconv curl nodejs npm git php5-bcmath php5-ctype php5-xmlreader php5-zip php5-ldap php5-opcache \
              php5-pear php5-dev \
              zip unzip \
              postfix ca-certificates tzdata curl openssh-client libltdl shadow util-linux pciutils   usbutils  \
              coreutils  binutils    findutils  grep  bash bash-completion gettext"
ENV PHPIZE_DEPS="git \
    file \
    re2c \
    autoconf \
    make \
    zlib \
    zlib-dev \
    g++ \
    libmemcached \
    libmemcached-libs \
    libmemcached-dev \
    build-base \
    zlib-dev \
    php5-dev \
    git \
    autoconf \
    cyrus-sasl-dev \
    gcc \
    musl-dev \
    make"


#ENV PHP_DEPS="php5@community \
#    php5-dev@community \
#    php5-bcmath@community \
#    # but you can avoid pecl installation by this method \
#    php5-xdebug@community"

RUN set -xe; \
    apk add --update --no-cache --virtual .build-apache ${PHP5_DEPS}; \
    apk add --update --no-cache --virtual .build-deps ${PHPIZE_DEPS};
#
#    \
#        curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
#    \
#    # or pecl installation
#    pecl install xdebug; \
#    # clean up (remove build packages)
#    apk del .build-deps;




ADD serpro-ca/root_1.crt /usr/local/share/ca-certificates/root_1.crt
ADD serpro-ca/root_2.crt /usr/local/share/ca-certificates/root_2.crt
ADD serpro-ca/root_3.crt /usr/local/share/ca-certificates/root_3.crt
ADD icmbio-ca/CA_ICMBIO.crt /usr/local/share/ca-certificates/CA_ICMBIO.crt

RUN update-ca-certificates

# Create directories
RUN mkdir -p /var/www/files /run/apache2  /run/supervisord/ && \
    chown -R apache:apache /var/www/* && \
    # Prepare crontab
    # Prepare httpd.conf
    sed -i -e '\#<Directory />#,\#</Directory>#d' /etc/apache2/httpd.conf && \
    sed -i -e "s/^ServerSignature.*/ServerSignature Off/" /etc/apache2/httpd.conf
    # Clear the image
#RUN apk del --no-cache nodejs git
#RUN sed -i "s|include_path = \".:/usr/share/php7\"|include_path = \".:/usr/share/php7:${APP_HOME}/vendor/zendframework/zendframework1/library\"|" /etc/php7/php.ini
COPY conf/alpine/ /

EXPOSE 80 443

CMD ["/usr/bin/supervisord", "-c", "/etc/supervisord.conf"]


FROM develop AS build

FROM develop AS production
